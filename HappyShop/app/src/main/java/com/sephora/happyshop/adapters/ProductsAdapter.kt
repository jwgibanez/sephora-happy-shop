package com.sephora.happyshop.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sephora.happyshop.R
import com.sephora.happyshop.database.models.Product
import kotlinx.android.synthetic.main.list_item_product.view.*

class ProductsAdapter(val itemClick: (Product) -> Unit): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: ArrayList<Product>

    init {
        items = ArrayList<Product>()
    }

    fun setItems(list: ArrayList<Product>) {
        items = list
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        (holder as ViewHolder).bind(items.get(position), itemClick)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_product, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(product: Product, itemClick: (Product) -> Unit) = with(itemView) {
            Glide.with(this.context).load(product.getImgUrl()).into(itemView.thumb)
            itemView.title.setText(product.getName())
            itemView.price.setText(String.format("S$%.0f", product.getPrice()))
            itemView.sale.setText(if (product.isUnderSale()) "ON SALE" else "")
            itemView.product_list_card.setOnClickListener { itemClick(product) }
        }
    }

}