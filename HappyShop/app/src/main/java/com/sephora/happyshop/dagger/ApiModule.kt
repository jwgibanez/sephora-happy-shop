package com.sephora.happyshop.dagger

import com.sephora.happyshop.network.ApiClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by jwgibanez on 24/7/17.
 */

@Module
class ApiModule {
    @Provides @Singleton fun provideApiClient() = ApiClient()
}
