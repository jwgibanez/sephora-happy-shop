package com.sephora.happyshop.network.responses

import com.sephora.happyshop.database.models.Product

/**
 * Created by jwgibanez on 25/7/17.
 */

class GetProductResponse {
    private var product: Product? = null

    fun getProduct(): Product? {
        return product
    }
}
