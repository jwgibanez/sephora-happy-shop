package com.sephora.happyshop.database.models;

import java.io.Serializable;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by jwgibanez on 25/7/17.
 */

public class Product extends RealmObject implements Serializable {

    @Ignore
    private static final String COLUMN_ID = "id";

    @PrimaryKey
    private int id;

    private String name;
    private String description;
    private String category;
    private float price;
    private String img_url;
    private boolean under_sale;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getCategory() {
        return category;
    }

    public float getPrice() {
        return price;
    }

    public String getImgUrl() {
        return img_url;
    }

    public boolean isUnderSale() {
        return under_sale;
    }

    public static void addProduct(Product product) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(product);
        realm.commitTransaction();
    }

    public static Product getProduct(int id) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Product> query = realm.where(Product.class).equalTo(COLUMN_ID, id);
        RealmResults<Product> results = query.findAll();
        List<Product> list = realm.copyFromRealm(results);
        if (list.size() > 0)
            return list.get(0);
        else
            return null;
    }
}
