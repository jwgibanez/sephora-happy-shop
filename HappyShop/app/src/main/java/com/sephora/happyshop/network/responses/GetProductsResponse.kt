package com.sephora.happyshop.network.responses

import com.sephora.happyshop.database.models.Product

/**
 * Created by jwgibanez on 24/7/17.
 */

class GetProductsResponse {
    private var products: ArrayList<Product> = ArrayList<Product>()

    fun getProducts(): ArrayList<Product> {
        return products
    }
}