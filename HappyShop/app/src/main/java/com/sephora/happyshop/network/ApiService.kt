package com.sephora.happyshop.network

import com.sephora.happyshop.network.responses.GetCategoriesResponse
import com.sephora.happyshop.network.responses.GetProductResponse
import com.sephora.happyshop.network.responses.GetProductsResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by jwgibanez on 24/7/17.
 */

interface ApiService {
    @GET("/")
    fun test(): Call<String>;

    @GET("/api/v1/categories.json")
    fun getCategories(): Call<GetCategoriesResponse>;

    @GET("/api/v1/products.json")
    fun getProducts(@Query("category") category: String, @Query("page") page:Int): Call<GetProductsResponse>;

    @GET("/api/v1/products/{id}.json/")
    fun getProduct(@Path("id") id: Int): Call<GetProductResponse>;
}