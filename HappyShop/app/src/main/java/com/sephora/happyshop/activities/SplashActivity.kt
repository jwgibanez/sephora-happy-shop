package com.sephora.happyshop.activities

import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.os.Bundle

import com.sephora.happyshop.R

/**
 * Created by jwgibanez on 7/24/17.
 */

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({
            MainActivity.start(this)
            finish()
        }, 2000)
    }
}
