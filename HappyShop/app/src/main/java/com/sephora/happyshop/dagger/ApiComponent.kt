package com.sephora.happyshop.dagger

import dagger.Component
import javax.inject.Singleton

/**
 * Created by jwgibanez on 24/7/17.
 */

@Singleton
@Component(modules = arrayOf(ApiModule::class))
interface ApiComponent {
    fun inject(activity: ApiActivity)
    fun inject(activity: ApiFragment)
}