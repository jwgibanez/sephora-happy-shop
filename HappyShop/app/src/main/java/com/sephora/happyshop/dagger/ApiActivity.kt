package com.sephora.happyshop.dagger

import android.support.v7.app.AppCompatActivity
import com.sephora.happyshop.network.ApiClient
import javax.inject.Inject

/**
 * Created by jwgibanez on 24/7/17.
 */

open class ApiActivity: AppCompatActivity() {
    @Inject lateinit var apiClient: ApiClient
}
