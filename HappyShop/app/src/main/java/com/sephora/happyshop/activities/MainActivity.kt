package com.sephora.happyshop.activities

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.Toolbar
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.sephora.happyshop.MainApplication

import com.sephora.happyshop.R
import com.sephora.happyshop.dagger.ApiActivity
import com.sephora.happyshop.database.models.Category
import com.sephora.happyshop.fragments.CategoriesFragment
import com.sephora.happyshop.fragments.MyCartFragment
import android.net.ConnectivityManager
import android.support.v4.view.GravityCompat
import android.view.View
import com.sephora.happyshop.database.models.Cart
import io.realm.Realm

/**
 * Created by jwgibanez on 7/24/17.
 */

class MainActivity : ApiActivity() {

    companion object {
        const val REQUEST_LAUNCH = 8990

        val categories = ArrayList<Category>()
        lateinit var progress: ProgressDialog
        fun start(activity : Activity) {
            val intent = Intent(activity, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            activity.startActivityForResult(intent, REQUEST_LAUNCH)
        }
        fun hasConnection(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE)
                    as ConnectivityManager
            val activeNetworkInfo = connectivityManager.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnected
        }
        fun showProgress(message: String) {
            progress.setMessage(message)
            progress.show()
        }
        fun dismissProgress() {
            progress.dismiss()
        }
    }

    @BindView(R.id.toolbar)
    lateinit var main_toolbar: Toolbar

    @BindView(R.id.toolbar_title)
    lateinit var main_toolbar_title: TextView

    @BindView(R.id.drawer_layout)
    lateinit var drawer_layout: DrawerLayout

    @BindView(R.id.cart_count)
    lateinit var cart_count: TextView

    lateinit var drawer_toggle: ActionBarDrawerToggle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MainApplication.apiComponent.inject(this)
        ButterKnife.bind(this)
        setToolbar()
        updateCartBadge()
        progress = ProgressDialog(this)
        drawer_toggle = ActionBarDrawerToggle(this, drawer_layout, getToolbar(),
                R.string.app_name, R.string.app_name)
        drawer_toggle.setDrawerIndicatorEnabled(true)
        drawer_layout.setDrawerListener(drawer_toggle)
        drawer_toggle.syncState()
        categories()
    }

    private fun setToolbar() {
        setSupportActionBar(main_toolbar)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false)
            val intent = parentActivityIntent
            if (intent != null) {
                actionBar.setDisplayHomeAsUpEnabled(true)
                actionBar.setHomeButtonEnabled(true)

                val home = findViewById(android.R.id.home) as ImageView
                home.setColorFilter(Color.WHITE)
            }
        }
        setToolbarTitle(getString(R.string.app_name))
    }

    fun setToolbarTitle(title: String) {
        main_toolbar_title.setText(title)
    }

    fun getToolbar(): Toolbar {
        return main_toolbar
    }

    @OnClick(R.id.drawer_categories)
    fun categories() {
        drawer_layout.closeDrawers()
        val fragment = supportFragmentManager.findFragmentById(R.id.content)
        if (fragment == null || fragment !is CategoriesFragment) {
            clearStackAndReplaceFragment(CategoriesFragment())
        }
    }

    @OnClick(R.id.drawer_my_cart)
    fun myCart() {
        drawer_layout.closeDrawers()
        val fragment = supportFragmentManager.findFragmentById(R.id.content)
        if (fragment == null || fragment !is MyCartFragment) {
            clearStackAndReplaceFragment(MyCartFragment())
        }
    }

    @OnClick(R.id.cart)
    fun myCartBadge() {
        myCart()
    }

    fun clearStackAndReplaceFragment(fragment: Fragment) {
        for (i in 0..(supportFragmentManager.backStackEntryCount - 1)) {
            supportFragmentManager.popBackStack()
        }
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.content, fragment)
                .commit()
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawers()
        } else if (supportFragmentManager.backStackEntryCount == 0) {
            // Minimize app.
            val startMain = Intent(Intent.ACTION_MAIN)
            startMain.addCategory(Intent.CATEGORY_HOME)
            startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(startMain)
        } else {
            super.onBackPressed()
        }
    }

    fun updateCartBadge() {
        val items = Cart.getCartItems()
        cart_count.setText(items.size.toString())
        if (items.size > 0) {
            cart_count.visibility = View.VISIBLE
        } else {
            cart_count.visibility = View.GONE
        }
    }

    fun clearDb() {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        realm.deleteAll()
        realm.commitTransaction()
    }
}
