package com.sephora.happyshop.tools

import android.app.AlertDialog
import android.content.Context
import android.text.TextUtils
import android.view.View
import android.view.Window
import android.view.WindowManager
import com.sephora.happyshop.R

/**
 * Created by jwgibanez on 24/7/17.
 */

object DialogUtils {

    private var mAlertDialog: AlertDialog? = null

    interface Callback {
        fun onPositiveClicked()
        fun onNegativeClicked()
    }

    fun showAlert(context: Context, message: String) {
        show(context, null, message, true, context.getString(R.string.ok), null, object : Callback {
            override fun onPositiveClicked() {

            }

            override fun onNegativeClicked() {

            }
        })
    }

    fun showAlert(context: Context, message: String, callback: Callback) {
        show(context, null, message, true, context.getString(R.string.ok), null, callback)
    }

    fun showAlert(context: Context, title: String, message: String,
                  callback: Callback) {
        show(context, title, message, true, context.getString(R.string.ok), null, callback)
    }

    fun showAlert(context: Context, title: String, message: String,
                  buttonText: String, callback: Callback) {
        show(context, title, message, true, buttonText, null, callback)
    }

    fun showAlert(context: Context, title: String, message: String, isCancelable: Boolean,
                  buttonText: String, callback: Callback) {
        show(context, title, message, isCancelable, buttonText, null, callback)
    }

    fun showChoice(context: Context, message: String,
                   callback: Callback) {
        showChoice(context, null, message, context.getString(R.string.ok),
                context.getString(R.string.cancel), callback)
    }

    fun showChoice(context: Context, title: String, message: String,
                   callback: Callback) {
        showChoice(context, title, message, context.getString(R.string.ok),
                context.getString(R.string.cancel), callback)
    }

    fun showChoice(context: Context, title: String, message: String,
                   positiveButtonText: String,
                   callback: Callback) {
        showChoice(context, title, message, positiveButtonText,
                context.getString(R.string.cancel), callback)
    }

    fun showChoice(context: Context, title: String, message: String, isCancelable: Boolean,
                   positiveButtonText: String,
                   callback: Callback) {
        show(context, title, message, isCancelable, positiveButtonText,
                context.getString(R.string.cancel), callback)
    }

    fun showChoice(context: Context, title: String?, message: String,
                   positiveButtonText: String, negativeButtonText: String,
                   callback: Callback) {
        show(context, title, message, true, positiveButtonText, negativeButtonText, callback)
    }

    fun show(context: Context, title: String?, message: String, isCancelable: Boolean,
             positiveButtonText: String, negativeButtonText: String?,
             callback: Callback) {
        show(context, title, message, null, isCancelable, positiveButtonText, negativeButtonText, callback)
    }

    fun show(context: Context, title: String?, message: String?, view: View?, isCancelable: Boolean,
             positiveButtonText: String, negativeButtonText: String?,
             callback: Callback?) {
        val builder = AlertDialog.Builder(context)

        builder
                .setTitle(title)
                .setCancelable(isCancelable)

        if (message != null) {
            builder.setMessage(message)
        }

        if (view != null) {
            builder.setView(view)
        }

        if (!TextUtils.isEmpty(positiveButtonText) && callback != null) {
            builder.setPositiveButton(positiveButtonText) { dialog, which ->
                if (view == null) {
                    dialog.dismiss()
                }
                callback.onPositiveClicked()
            }
        }

        if (!TextUtils.isEmpty(negativeButtonText) && callback != null) {
            builder.setNegativeButton(negativeButtonText) { dialog, which ->
                if (view == null) {
                    dialog.dismiss()
                }
                callback.onNegativeClicked()
            }
        }

        if (mAlertDialog != null && mAlertDialog!!.isShowing) {
            mAlertDialog!!.dismiss()
        }

        mAlertDialog = builder.create()

        if (TextUtils.isEmpty(title)) {
            mAlertDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        }

        if (view != null) {
            mAlertDialog!!.window!!.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
        }

        mAlertDialog!!.show()

    }
}