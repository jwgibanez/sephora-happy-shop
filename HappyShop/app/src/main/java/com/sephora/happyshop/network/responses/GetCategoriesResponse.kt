package com.sephora.happyshop.network.responses

import com.sephora.happyshop.database.models.Category

/**
 * Created by jwgibanez on 24/7/17.
 */

class GetCategoriesResponse {
    private var categories: ArrayList<Category> = ArrayList<Category>()

    fun getCategories(): ArrayList<Category> {
        return categories
    }
}