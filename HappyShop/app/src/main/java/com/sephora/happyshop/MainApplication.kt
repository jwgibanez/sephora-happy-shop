package com.sephora.happyshop

import android.app.Activity
import android.app.Application
import android.os.Bundle
import com.sephora.happyshop.dagger.ApiComponent
import com.sephora.happyshop.dagger.DaggerApiComponent
import com.sephora.happyshop.database.DbHelper

/**
 * Created by jwgibanez on 24/7/17.
 */

class MainApplication : Application(), Application.ActivityLifecycleCallbacks  {
    companion object {
        lateinit var apiComponent: ApiComponent
    }

    var currentActivity: Activity? = null

    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(this)
        apiComponent = DaggerApiComponent.builder().build()
        DbHelper.init(this)
    }

    override fun onActivityPaused(activity: Activity?) {

    }

    override fun onActivityResumed(activity: Activity) {
        currentActivity = activity
    }

    override fun onActivityStarted(activity: Activity) {
        currentActivity = activity
    }

    override fun onActivityDestroyed(activity: Activity?) {

    }

    override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {

    }

    override fun onActivityStopped(activity: Activity?) {

    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        currentActivity = activity
    }
}