package com.sephora.happyshop.network

import android.text.TextUtils
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.URL

/**
 * Created by jwgibanez on 24/7/17.
 */

class ApiClient {
    companion object {
        private var BASE_URL = "http://sephora-mobile-takehome-apple.herokuapp.com/"
        lateinit var mService: ApiService
        lateinit var mRestAdapter: Retrofit
        fun setBaseUrl(baseUrl: String) {
            BASE_URL = baseUrl
        }
        fun getBaseUrl(): String {
            return BASE_URL
        }
    }

    init {
        initializeService()
    }

    fun initializeService() {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)

        val httpCLient = OkHttpClient.Builder()
        httpCLient.addInterceptor(logging)

        val gson = GsonBuilder().setLenient().create()

        mRestAdapter = Retrofit.Builder()
                .baseUrl(getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpCLient.build())
                .build()

        mService = mRestAdapter.create(ApiService::class.java)
    }

    fun getService(): ApiService {
        val currentHost = mRestAdapter.baseUrl().host()
        val baseHost = URL(getBaseUrl()).host
        if (!TextUtils.equals(currentHost, baseHost)) {
            initializeService()
        }

        return mService
    }
}
