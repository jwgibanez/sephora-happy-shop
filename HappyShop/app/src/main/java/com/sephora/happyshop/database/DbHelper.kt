package com.sephora.happyshop.database

import android.content.Context
import io.realm.Realm
import io.realm.RealmConfiguration

/**
 * Created by jwgibanez on 24/7/17.
 */

object DbHelper {
    private val REALM_DEFAULT_NAME = "happyshop.realm"
    private val REALM_DEFAULT_KEY = "c2kEohYHJ49o84Us11Fu4hrVouGIjMvaycHVpjeW337WJq0c5j6yfhYgwECxEKsA"

    private val REALM_DEFAULT_SCHEMA_VERSION: Long = 1

    fun init(context: Context) {
        Realm.init(context)
        val configuration = getDefaultConfig(context)
        Realm.setDefaultConfiguration(configuration)
    }

    fun getDefaultConfig(context: Context): RealmConfiguration {
        return RealmConfiguration.Builder()
                .name(REALM_DEFAULT_NAME)
                .encryptionKey(REALM_DEFAULT_KEY.toByteArray())
                .schemaVersion(REALM_DEFAULT_SCHEMA_VERSION)
                .build()
    }
}