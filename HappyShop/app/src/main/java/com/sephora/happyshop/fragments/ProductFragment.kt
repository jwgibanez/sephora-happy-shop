package com.sephora.happyshop.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.sephora.happyshop.R
import com.sephora.happyshop.activities.MainActivity
import com.sephora.happyshop.database.models.Cart
import com.sephora.happyshop.database.models.Product
import kotlinx.android.synthetic.main.fragment_product.view.*

/**
 * Created by jwgibanez on 25/7/17.
 */

class ProductFragment : Fragment() {
    companion object {
        const val EXTRA_PRODUCT = "extra_product"
        fun getBundle(product: Product): Bundle {
            val bundle = Bundle()
            bundle.putSerializable(EXTRA_PRODUCT, product)
            return bundle
        }
    }

    lateinit var product: Product

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        product = arguments.getSerializable(EXTRA_PRODUCT) as Product
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_product, container, false)
        ButterKnife.bind(this, view)
        Glide.with(this.context).load(product.getImgUrl()).into(view.product_thumb)
        view.product_title.setText(product.getName())
        view.product_price.setText(String.format("S$%.0f", product.getPrice()))
        view.product_sale.setText(if (product.isUnderSale()) "ON SALE" else "")
        view.product_description.setText(product.getDescription())
        initButton(view)
        return view
    }

    fun initButton(view: View?) {
        if (view != null) {
            if (Cart.getCartItem(product.getId()) != null) {
                view.product_cart_button.setText(getString(R.string.remove_from_cart))
                view.product_cart_button.setBackgroundColor(
                        ContextCompat.getColor(activity, android.R.color.holo_red_dark))
            } else {
                view.product_cart_button.setText(getString(R.string.add_to_cart))
                view.product_cart_button.setBackgroundColor(
                        ContextCompat.getColor(activity,android.R.color.holo_green_dark))
            }
        }
    }

    @OnClick(R.id.product_cart_button)
    fun cart() {
        if (Cart.getCartItem(product.getId()) != null) {
            Cart.removeFromCart(product.getId())
        } else {
            Cart.addToCart(product)
        }
        initButton(view)
        (activity as MainActivity).updateCartBadge()
    }
}