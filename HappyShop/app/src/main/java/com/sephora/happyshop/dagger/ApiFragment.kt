package com.sephora.happyshop.dagger

import android.support.v4.app.Fragment
import com.sephora.happyshop.network.ApiClient
import javax.inject.Inject

/**
 * Created by jwgibanez on 24/7/17.
 */

open class ApiFragment: Fragment() {
    @Inject lateinit var apiClient: ApiClient
}
