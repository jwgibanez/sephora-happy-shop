package com.sephora.happyshop.database.models

import java.io.Serializable

/**
 * Created by jwgibanez on 24/7/17.
 */

class Category : Serializable {
    private var name: String = ""
    private var products_count: Int = 0

    fun getName(): String {
        return name
    }

    fun getProductsCount(): Int {
        return products_count
    }
}