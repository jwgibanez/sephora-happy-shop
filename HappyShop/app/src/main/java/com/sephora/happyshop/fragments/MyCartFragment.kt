package com.sephora.happyshop.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.sephora.happyshop.R
import com.sephora.happyshop.activities.MainActivity
import com.sephora.happyshop.adapters.CartAdapter
import com.sephora.happyshop.database.models.Cart
import com.sephora.happyshop.database.models.Product
import com.sephora.happyshop.tools.DialogUtils

/**
 * Created by jwgibanez on 7/24/17.
 */

class MyCartFragment : Fragment() {

    @BindView(R.id.recycler_view_cart)
    lateinit var recyclerView: RecyclerView

    @BindView(R.id.checkout)
    lateinit var checkout: Button

    lateinit var adapter: CartAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_cart, container, false)
        ButterKnife.bind(this, view)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        if (recyclerView.adapter == null) {
            adapter = CartAdapter({
                product ->
                val fragment = ProductFragment()
                fragment.arguments = ProductFragment.getBundle(product)
                this@MyCartFragment.activity.supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.content, fragment)
                        .addToBackStack(null)
                        .commit()
            }) {
                product ->
                DialogUtils.showChoice(activity,
                        getString(R.string.choice_remove_from_cart),
                        object : DialogUtils.Callback {
                            override fun onPositiveClicked() {
                                Cart.removeFromCart(product.id)
                                refreshCart()
                                (activity as MainActivity).updateCartBadge()
                            }

                            override fun onNegativeClicked() {

                            }
                        })

            }
            refreshCart()
            recyclerView.adapter = adapter
        }
        return view;
    }

    fun refreshCart() {
        val cartItems = Cart.getCartItems()
        val products = ArrayList<Product>()
        cartItems.forEach {
            val product =  Product.getProduct(it.id)
            if (product != null) {
                products.add(product)
            }
        }
        adapter.setItems(products)
        adapter.notifyDataSetChanged()
        if (products.size < 1) {
            DialogUtils.showAlert(activity, getString(R.string.empty_cart))
            checkout.visibility = View.GONE
        } else {
            checkout.visibility = View.VISIBLE
        }
    }

    @OnClick(R.id.checkout)
    fun checkout() {
        Toast.makeText(activity, "Checkout: Coming Soon!", Toast.LENGTH_LONG).show()
    }
}
