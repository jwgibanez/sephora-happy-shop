package com.sephora.happyshop.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import butterknife.BindView
import butterknife.ButterKnife
import com.sephora.happyshop.MainApplication
import com.sephora.happyshop.R
import com.sephora.happyshop.activities.MainActivity
import com.sephora.happyshop.adapters.CategoriesAdapter
import com.sephora.happyshop.dagger.ApiFragment
import com.sephora.happyshop.network.responses.GetCategoriesResponse
import com.sephora.happyshop.tools.DialogUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by jwgibanez on 7/24/17.
 */

class CategoriesFragment : ApiFragment() {

    @BindView(R.id.recycler_view_categories)
    lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.apiComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_categories, container, false)
        ButterKnife.bind(this, view)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        if (recyclerView.adapter == null) {
            val adapter = CategoriesAdapter() {
                category ->
                val fragment = ProductsFragment()
                fragment.arguments = ProductsFragment.getBundle(category)
                activity.supportFragmentManager
                        .beginTransaction()
                        .add(R.id.content, fragment)
                        .addToBackStack(null)
                        .commit()
            }
            adapter.setItems(MainActivity.categories)
            recyclerView.adapter = adapter
        }
        return view;
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (MainActivity.hasConnection(activity)) {
            MainActivity.showProgress(getString(R.string.loading))
            apiClient.getService().getCategories().enqueue(object : Callback<GetCategoriesResponse> {
                override fun onFailure(call: Call<GetCategoriesResponse>?, t: Throwable?) {
                    MainActivity.dismissProgress()
                }

                override fun onResponse(call: Call<GetCategoriesResponse>?,
                                        response: Response<GetCategoriesResponse>) {
                    MainActivity.dismissProgress()
                    val data = response.body()
                    if (data != null) {
                        MainActivity.categories.clear()
                        MainActivity.categories.addAll(data.getCategories())
                        recyclerView.adapter.notifyDataSetChanged()
                    }
                }
            })
        } else {
            DialogUtils.showAlert(activity,
                    getString(R.string.error_no_connection))
            recyclerView.adapter.notifyDataSetChanged()
        }
    }
}