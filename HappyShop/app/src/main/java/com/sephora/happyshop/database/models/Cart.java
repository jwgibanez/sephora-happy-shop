package com.sephora.happyshop.database.models;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by jwgibanez on 25/7/17.
 */

public class Cart extends RealmObject {

    @Ignore
    private static final String COLUMN_ID = "id";

    @PrimaryKey
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static void addToCart(Product product) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Cart item = new Cart();
        item.setId(product.getId());
        realm.copyToRealmOrUpdate(item);
        realm.commitTransaction();
    }

    public static void removeFromCart(int id) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        RealmQuery<Cart> query = realm.where(Cart.class).equalTo(COLUMN_ID, id);
        RealmResults<Cart> results = query.findAll();
        if (results.size() > 0) {
            results.deleteAllFromRealm();
        }
        realm.commitTransaction();
    }

    public static Cart getCartItem(int id) {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Cart> query = realm.where(Cart.class).equalTo(COLUMN_ID, id);
        RealmResults<Cart> results = query.findAll();
        List<Cart> list = realm.copyFromRealm(results);
        if (list.size() > 0)
            return list.get(0);
        else
            return null;
    }

    public static List<Cart> getCartItems() {
        Realm realm = Realm.getDefaultInstance();
        RealmQuery<Cart> query = realm.where(Cart.class);
        RealmResults<Cart> results = query.findAll();
        return realm.copyFromRealm(results);
    }
}
