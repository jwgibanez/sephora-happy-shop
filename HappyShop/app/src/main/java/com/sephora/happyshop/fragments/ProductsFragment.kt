package com.sephora.happyshop.fragments

import android.graphics.Color
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import bolts.Task
import bolts.TaskCompletionSource
import butterknife.BindView
import butterknife.ButterKnife
import com.sephora.happyshop.MainApplication
import com.sephora.happyshop.R
import com.sephora.happyshop.activities.MainActivity
import com.sephora.happyshop.adapters.ProductsAdapter
import com.sephora.happyshop.dagger.ApiFragment
import com.sephora.happyshop.database.models.Category
import com.sephora.happyshop.database.models.Product
import com.sephora.happyshop.network.responses.GetProductResponse
import com.sephora.happyshop.network.responses.GetProductsResponse
import com.sephora.happyshop.tools.DialogUtils
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.widget.TextView
import android.support.v4.content.ContextCompat
import com.androidadvance.topsnackbar.TSnackbar
import android.view.Gravity

/**
 * Created by jwgibanez on 25/7/17.
 */

class ProductsFragment : ApiFragment() {
    companion object {
        const val EXTRA_CATEGORY = "extra_category"
        const val PAGE_SIZE = 10
        fun getBundle(category: Category): Bundle {
            val bundle = Bundle()
            bundle.putSerializable(EXTRA_CATEGORY, category)
            return bundle
        }
    }

    @BindView(R.id.recycler_view_products)
    lateinit var recyclerView: RecyclerView

    lateinit var category: Category
    lateinit var adapter: ProductsAdapter
    lateinit var snackbar: TSnackbar
    val products = HashMap<Int, List<Product>>()
    var loading = false;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MainApplication.apiComponent.inject(this)
        category = arguments.getSerializable(EXTRA_CATEGORY) as Category
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_products, container, false)
        ButterKnife.bind(this, view)
        initLoadingMoreTSnackbar()
        recyclerView.layoutManager = GridLayoutManager(activity, 2)
        if (recyclerView.adapter == null) {
            adapter = ProductsAdapter() {
                product ->
                if (MainActivity.hasConnection(activity)) {
                    getProduct(product.getId())
                } else {
                    DialogUtils.showAlert(activity,
                            getString(R.string.error_no_connection))
                    recyclerView.adapter.notifyDataSetChanged()
                }
            }
            refreshList()
            recyclerView.adapter = adapter
            recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    if(dy > 0) { //check for scroll down
                        val totalItemCount = recyclerView.layoutManager.getItemCount();
                        val pastVisiblesItems = (recyclerView.layoutManager as GridLayoutManager)
                                .findFirstVisibleItemPosition();
                        if (!loading) {
                            // Load two pages in advance
                            if (totalItemCount <= (pastVisiblesItems + (PAGE_SIZE * 2))) {
                                val pages = (category.getProductsCount() / PAGE_SIZE) + 1
                                val nextPageToLoad = products.size + 1;
                                if (nextPageToLoad <= pages && products.get(nextPageToLoad) == null) {
                                    showLoadingMoreTSnackbar()
                                    getProductsPage(nextPageToLoad).continueWith {
                                        task: Task<Void> ->
                                        hideLoadingMoreTSnackbar()
                                        if (!task.isFaulted) {
                                            refreshList()
                                        } else {
                                            DialogUtils.showAlert(
                                                    activity,
                                                    getString(R.string.error_generic))
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                override fun onScrollStateChanged(recyclerView: RecyclerView?, newState: Int) {
                    super.onScrollStateChanged(recyclerView, newState)
                }
            })
        }
        return view;
    }

    fun initLoadingMoreTSnackbar() {
        snackbar = TSnackbar.make(activity.findViewById(R.id.container),
                getString(R.string.loading_more_products), TSnackbar.LENGTH_INDEFINITE)
        snackbar.setMaxWidth(0)
        val snackbarView = snackbar.getView()
        snackbarView.setBackgroundColor(ContextCompat.getColor(activity,
                android.R.color.holo_blue_dark))
        val textView = snackbarView
                .findViewById(com.androidadvance.topsnackbar.R.id.snackbar_text) as TextView
        textView.setTextColor(Color.WHITE)
        textView.gravity = Gravity.CENTER
    }

    fun showLoadingMoreTSnackbar() {
        if (!snackbar.isShown) {
            snackbar.show()
        }
    }

    fun hideLoadingMoreTSnackbar() {
        snackbar.dismiss()
    }

    fun refreshList() {
        val all = ArrayList<Product>()
        for (page: Int in 1..products.size) {
            all.addAll(products.get(page) as List<Product>)
        }
        adapter.setItems(all)
        adapter.notifyDataSetChanged()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (MainActivity.hasConnection(activity)) {
            MainActivity.showProgress(getString(R.string.loading))
            /*// This block of code loads the first page only
            getProductsPage(1).continueWith {
                task: Task<Void> ->
                MainActivity.dismissProgress()
                if (!task.isFaulted) {
                    refreshList()
                } else {
                    DialogUtils.showAlert(activity, getString(R.string.error_generic))
                }
            }*/
            // This block of code tries to load all/some of the known pages
            val pages = (category.getProductsCount() / PAGE_SIZE) + 1
            val tasks = ArrayList<Task<Void>>()
            for (page in 1..3) {
                // Load 3 pages initially, if possible
                if (page <= pages)
                    tasks.add(getProductsPage(page))
            }

            Task.whenAll(tasks).continueWith {
                task: Task<Void> ->
                MainActivity.dismissProgress()
                if (!task.isFaulted) {
                    refreshList()
                } else {
                    DialogUtils.showAlert(activity, getString(R.string.error_generic))
                }
            }
        } else {
            DialogUtils.showAlert(activity,
                    getString(R.string.error_no_connection))
            recyclerView.adapter.notifyDataSetChanged()
        }
    }

    fun getProductsPage(page: Int): Task<Void> {
        val source = TaskCompletionSource<Void>()
        loading = true
        apiClient.getService().getProducts(category.getName(), page)
                .enqueue(object : Callback<GetProductsResponse> {
                    override fun onResponse(call: Call<GetProductsResponse>,
                                            response: Response<GetProductsResponse>) {
                        loading = false
                        val data = response.body()
                        if (data != null) {
                            products.put(page, data.getProducts())
                        }
                        source.setResult(null)
                    }
                    override fun onFailure(call: Call<GetProductsResponse>, t: Throwable) {
                        loading = false
                        source.setError(Exception(t.message))
                    }
                })
        return source.task;
    }

    fun getProduct(id: Int): Task<Product> {
        val source = TaskCompletionSource<Product>()
        apiClient.getService().getProduct(id)
                .enqueue(object : Callback<GetProductResponse> {
                    override fun onResponse(call: Call<GetProductResponse>,
                                            response: Response<GetProductResponse>) {
                        if (response.isSuccessful) {
                            val product = response.body()?.getProduct()
                            if (product != null) {
                                Product.addProduct(product)
                                val fragment = ProductFragment()
                                fragment.arguments = ProductFragment.getBundle(product)
                                this@ProductsFragment.activity.supportFragmentManager
                                        .beginTransaction()
                                        .add(R.id.content, fragment)
                                        .addToBackStack(null)
                                        .commit()
                                return
                            }
                        }
                        source.setError(Exception(response.errorBody().toString()))
                    }

                    override fun onFailure(call: Call<GetProductResponse>, t: Throwable) {
                        source.setError(Exception(t.message))
                    }
                })
        return source.task;
    }
}
