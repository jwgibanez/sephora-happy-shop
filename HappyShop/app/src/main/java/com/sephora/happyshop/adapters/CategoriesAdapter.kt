package com.sephora.happyshop.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sephora.happyshop.R
import com.sephora.happyshop.database.models.Category
import kotlinx.android.synthetic.main.list_item_category.view.*

class CategoriesAdapter(val itemClick: (Category) -> Unit): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var items: ArrayList<Category>

    init {
        items = ArrayList<Category>()
    }

    fun setItems(list: ArrayList<Category>) {
        items = list
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        (holder as ViewHolder).bind(items.get(position), itemClick)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_category, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        fun bind(category: Category, itemClick: (Category) -> Unit) = with(itemView) {
            itemView.category_name.setText(category.getName())
            val itemCnt = category.getProductsCount();
            itemView.category_items.setText("${itemCnt} item${if (itemCnt > 1) "s" else ""}")
            itemView.card.setOnClickListener { itemClick(category) }
        }
    }
}