package com.sephora.happyshop.instrumentedtests;

import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingPolicies;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.sephora.happyshop.R;
import com.sephora.happyshop.actions.ViewActions;
import com.sephora.happyshop.activities.MainActivity;
import com.sephora.happyshop.assertions.ListHasAtLeastOneItemAssertion;
import com.sephora.happyshop.assertions.ListHasNoItemAssertion;
import com.sephora.happyshop.resources.CategoriesListLoadedIdlingResource;
import com.sephora.happyshop.resources.ProductsListLoadedIdlingResource;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.registerIdlingResources;
import static android.support.test.espresso.Espresso.unregisterIdlingResources;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by jwgibanez on 7/25/17.
 */

@RunWith(AndroidJUnit4.class)
public class MainInstrumentedTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @Before
    public void clearDb() {
        mActivityRule.getActivity().clearDb();
    }

    @After
    public void tearDown() {
        List<IdlingResource> idlingResourceList = Espresso.getIdlingResources();
        if (idlingResourceList != null) {
            for (int i = 0; i < idlingResourceList.size(); i++) {
                unregisterIdlingResources(idlingResourceList.get(i));
            }
        }
    }

    @Test
    public void checkTitleBarText() throws Exception {
        Thread.sleep(1000);

        onView(withId(R.id.toolbar_title))
                .check(matches(isDisplayed()))
                .check(matches(withText("Happy Shop")));
    }

    @Test
    public void addAndRemoveProductInCart() throws Exception {
        IdlingPolicies.setMasterPolicyTimeout(1000 * 30, TimeUnit.MILLISECONDS);
        IdlingPolicies.setIdlingResourceTimeout(1000 * 30, TimeUnit.MILLISECONDS);

        // Categories list
        IdlingResource categoriesListIdlingResource = new CategoriesListLoadedIdlingResource();
        registerIdlingResources(categoriesListIdlingResource);

        Thread.sleep(2000);

        onView(withId(R.id.recycler_view_categories)).check(new ListHasAtLeastOneItemAssertion());

        // Click one category
        onView(withId(R.id.recycler_view_categories))
                .perform(RecyclerViewActions.scrollToPosition(0))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0,
                        ViewActions.clickChildViewWithId(R.id.card)));

        // Products list
        IdlingResource productsListIdlingResource = new ProductsListLoadedIdlingResource();
        registerIdlingResources(productsListIdlingResource);

        Thread.sleep(1000);

        onView(withId(R.id.recycler_view_products)).check(new ListHasAtLeastOneItemAssertion());

        // Click one product
        onView(withId(R.id.recycler_view_products))
                .perform(RecyclerViewActions.scrollToPosition(0))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0,
                        ViewActions.clickChildViewWithId(R.id.product_list_card)));

        Thread.sleep(2000);

        // Product info
        onView(withId(R.id.product_title)).check(matches(isDisplayed()));
        onView(withId(R.id.product_price)).check(matches(isDisplayed()));

        // Add to and remove from cart
        onView(withId(R.id.product_cart_button)).check(matches(isDisplayed()));
        onView(withId(R.id.product_cart_button)).check(matches(withText("Add To Cart")));
        onView(withId(R.id.product_cart_button)).perform(click());
        onView(withId(R.id.product_cart_button)).check(matches(withText("Remove From Cart")));
        onView(withId(R.id.product_cart_button)).perform(click());
    }

    @Test
    public void addProductToCartThenOpenCartViewRemoveFromCart() throws Exception {
        IdlingPolicies.setMasterPolicyTimeout(1000 * 30, TimeUnit.MILLISECONDS);
        IdlingPolicies.setIdlingResourceTimeout(1000 * 30, TimeUnit.MILLISECONDS);

        // Categories list
        IdlingResource categoriesListIdlingResource = new CategoriesListLoadedIdlingResource();
        registerIdlingResources(categoriesListIdlingResource);

        Thread.sleep(1000);

        onView(withId(R.id.recycler_view_categories)).check(new ListHasAtLeastOneItemAssertion());

        // Click one category
        onView(withId(R.id.recycler_view_categories))
                .perform(RecyclerViewActions.scrollToPosition(0))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0,
                        ViewActions.clickChildViewWithId(R.id.card)));

        // Products list
        IdlingResource productsListIdlingResource = new ProductsListLoadedIdlingResource();
        registerIdlingResources(productsListIdlingResource);

        Thread.sleep(1000);

        onView(withId(R.id.recycler_view_products)).check(new ListHasAtLeastOneItemAssertion());

        // Click one product
        onView(withId(R.id.recycler_view_products))
                .perform(RecyclerViewActions.scrollToPosition(0))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0,
                        ViewActions.clickChildViewWithId(R.id.product_list_card)));

        Thread.sleep(2000);

        // Product info
        onView(withId(R.id.product_title)).check(matches(isDisplayed()));
        onView(withId(R.id.product_price)).check(matches(isDisplayed()));

        // Add to cart
        onView(withId(R.id.product_cart_button)).check(matches(isDisplayed()));
        onView(withId(R.id.product_cart_button)).check(matches(withText("Add To Cart")));
        onView(withId(R.id.product_cart_button)).perform(click());

        Thread.sleep(2000);

        // Open cart
        //onView(withId(R.id.cart)).perform(click());
        mActivityRule.getActivity().myCart();

        Thread.sleep(1000);

        onView(withId(R.id.recycler_view_cart)).check(new ListHasAtLeastOneItemAssertion());

        // Remove from cart
        onView(withId(R.id.recycler_view_cart))
                .perform(RecyclerViewActions.scrollToPosition(0))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0,
                        ViewActions.clickChildViewWithId(R.id.product_cart_button)));

        // Confirm remove
        onView(withText("OK"))
                .inRoot(isDialog())
                .check(matches(isDisplayed()))
                .perform(click());

        Thread.sleep(1000);

        // Dismiss 'No item in cart' message
        onView(withText("OK"))
                .inRoot(isDialog())
                .check(matches(isDisplayed()))
                .perform(click());

        Thread.sleep(1000);

        onView(withId(R.id.recycler_view_cart)).check(new ListHasNoItemAssertion());
    }
}
