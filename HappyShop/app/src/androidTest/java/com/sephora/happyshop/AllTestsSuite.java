package com.sephora.happyshop;

import com.sephora.happyshop.instrumentedtests.MainInstrumentedTest;
import com.sephora.happyshop.instrumentedtests.PackageInstrumentedTest;
import com.sephora.happyshop.instrumentedtests.SplashInstrumentedTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by jwgibanez on 26/7/17.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({
        PackageInstrumentedTest.class,
        SplashInstrumentedTest.class,
        MainInstrumentedTest.class})
public class AllTestsSuite {
}
