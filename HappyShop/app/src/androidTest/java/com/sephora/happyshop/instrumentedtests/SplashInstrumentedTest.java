package com.sephora.happyshop.instrumentedtests;

import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.sephora.happyshop.R;
import com.sephora.happyshop.activities.SplashActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


/**
 * Created by jwgibanez on 7/25/17.
 */

@RunWith(AndroidJUnit4.class)
public class SplashInstrumentedTest {
    @Rule
    public ActivityTestRule<SplashActivity> mActivityRule = new ActivityTestRule<>(
            SplashActivity.class);

    @Test
    public void checkSplashText() {
        onView(ViewMatchers.withId(R.id.label))
                .check(matches(isDisplayed()))
                .check(matches(withText("Happy Shop")));
    }
}
