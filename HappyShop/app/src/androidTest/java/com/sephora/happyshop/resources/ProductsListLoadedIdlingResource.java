package com.sephora.happyshop.resources;

import android.app.Activity;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.IdlingResource;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.sephora.happyshop.MainApplication;
import com.sephora.happyshop.R;
import com.sephora.happyshop.activities.MainActivity;
import com.sephora.happyshop.adapters.ProductsAdapter;
import com.sephora.happyshop.fragments.ProductsFragment;

/**
 * Created by jwgibanez on 26/7/17.
 */

public class ProductsListLoadedIdlingResource implements IdlingResource {

    private ResourceCallback resourceCallback;
    private static boolean done = false;

    @Override
    public String getName() {
        return "Products list should have been loaded.";
    }

    @Override
    public boolean isIdleNow() {
        MainActivity activity = null;
        Fragment fragment = null;
        RecyclerView recyclerView = null;
        ProductsAdapter adapter = null;

        try {
            activity = (MainActivity) getCurrentActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (activity != null) {
            fragment = activity.getSupportFragmentManager().findFragmentById(R.id.content);
            recyclerView = (RecyclerView) activity.findViewById(R.id.recycler_view_products);
            if (recyclerView != null)
                adapter = (ProductsAdapter) recyclerView.getAdapter();
        }

        boolean isConditionMet = activity != null &&
                (fragment != null && fragment instanceof ProductsFragment) &&
                adapter != null && adapter.getItemCount() > 0;

        Log.d("Test-log: ", "isIdleNow() called, conditionStatus = " + isConditionMet);

        if (isConditionMet || done) {
            done = true;
            resourceCallback.onTransitionToIdle();
        }

        return isConditionMet || done;
    }

    private Activity getCurrentActivity() {
        return ((MainApplication) InstrumentationRegistry.
                getTargetContext().getApplicationContext()).getCurrentActivity();
    }

    @Override
    public void registerIdleTransitionCallback(
            ResourceCallback resourceCallback) {
        this.resourceCallback = resourceCallback;
    }
}
