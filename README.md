# Happy Shop #

Build a simplified Android shopping application with basic features. Let’s name this app the Happy Shop.

### Requirements ###

* User will be able to view products from at least 1 category.
* User will be able to view multiple products from a category view.
* After selecting a product from the category view, user will be able to view more detailed information of a product in the product view.
* User is able to add products to cart.
* Usage of REST-ful API endpoints to retrieve products information are required. They are documented below.
* Tests are included
* Support Android 6.0 (Marshmallow) and backwards compatible till at least Android 4.1 (Jelly Bean)
* Ensure minimal steps are required to run this project on other machines. Few basic instructions are welcomed.

### How do I get set up? ###

* The code runs on Android Studio and is written in Kotlin and Java.
* Kotlin: https://kotlinlang.org/docs/tutorials/kotlin-android.html
* Install Kotlin on Android Studio: https://blog.jetbrains.com/kotlin/2013/08/working-with-kotlin-in-android-studio/

### Views ###

#### Home view #####
Contains basic navigational elements for navigating to a category view. Note: Only 1 category has to be functional.

#### Category view ####
Contain a list of products and their information.
Request to the API should be paginated as user scrolls.
Some details that should be shown for each product: Image, price, on sale If a product is selected, the product view will be loaded.

#### Product view ####
Contain information about 1 specific product, including image.
Includes a call-to-action button (‘Add to cart’) for users to add product to cart.
Cart information should be stored locally. No. of items in cart must be shown in the Home and Category views if there are more than 1 items in the cart.

### REST-ful API endpoints ###

#### GET Products http://sephora-mobile-takehome-apple.herokuapp.com/api/v1/products.json ####

Parameters:
* category (mandatory)
* page (optional)

Returns multiple products with some information for each product.
Use page to paginate products by sets of 10.

#### GET Product http://sephora-mobile-takehome-apple.herokuapp.com/api/v1/products/id.json ####

Parameters:
* id (ID of product, mandatory)

Returns information for a single product.